require 'docker'

image_reference = 'gitlab/gitlab-ce:latest'

Docker::Image.create('fromImage' => image_reference)
container = Docker::Container.create(
  'name' => 'gitlab',
  'detach' => true,
  'Image' => image_reference,
  'HostConfig' => {
    'PortBindings' => {
      '80/tcp' => [{ 'HostPort' => '80' }],
      '443/tcp' => [{ 'HostPort' => '443' }]
    }
  }
)

container.start
